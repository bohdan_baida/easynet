﻿using IOC.Enums;
using IOC.Interfaces;
using IOC.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    public class Registry : IRegistry
    {
        private IList<RegistryItem> registries;

        public Registry()
        {
            registries = new List<RegistryItem>();
        }


        public RegistryItem AddTrancient<TAbstraction, TImplementation>()
        {
            return AddRegistry(ObjectLifetimesEnum.Trancient, typeof(TAbstraction), typeof(TImplementation));
        }

        public RegistryItem AddSingleton<TAbstraction, TImplementation>()
        {
            return AddRegistry(ObjectLifetimesEnum.Singleton, typeof(TAbstraction), typeof(TImplementation));
        }

        public RegistryItem GetRegistry(Type abstraction)
        {
            return registries.Where(c => c.AbstractionType == abstraction).FirstOrDefault();
        }

        public bool ContainsRegistry(Type abstraction)
        {
            return registries.Any(c => c.AbstractionType == abstraction);
        }

        public ITypeResolver GetResolver()
        {
            return new TypeResolver(this);
        }

        private RegistryItem AddRegistry(
            ObjectLifetimesEnum lifetime,
            Type abstractionType,
            Type implementationType)
        {
            var registry = new RegistryItem()
            {
                Lifetime = lifetime,
                AbstractionType = abstractionType,
                ImplementationType = implementationType,
            };

            ValidateRegistry(registry);

            registries.Add(registry);

            return registry;

        }

        private void ValidateRegistry(RegistryItem registry)
        {
            new RegistryValidator().Validate(registries, registry);
        }
    }
}
