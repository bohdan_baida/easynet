﻿namespace IOC.Interfaces
{
    public interface IContainer
    {
        TAbstraction Resolve<TAbstraction>();
    }
}
