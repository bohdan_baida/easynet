﻿using System;

namespace IOC.Interfaces
{
    public interface ITypeResolver
    {
        IRegistry Configuration { get; }
        object ResolveType(Type type);
    }
}
