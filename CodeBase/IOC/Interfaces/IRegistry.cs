﻿using System;

namespace IOC.Interfaces
{
    public interface IRegistry
    {
        RegistryItem AddTrancient<TAbstraction, TImplementation>();
        RegistryItem AddSingleton<TAbstraction, TImplementation>();
        RegistryItem GetRegistry(Type abstraction);
        bool ContainsRegistry(Type abstraction);
        ITypeResolver GetResolver();
    }
}
