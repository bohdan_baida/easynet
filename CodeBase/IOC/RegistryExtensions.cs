﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    public static class RegistryExtensions
    {
        public static RegistryItem Ctor(this RegistryItem registry, string name, object value)
        {
            registry.ConstructorParameters.Add(name, value);
            return registry;
        }
    }
}
