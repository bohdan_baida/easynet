﻿using IOC.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC.Validation.Interfaces
{
    interface IRegistryValidation : IValidation
    {
        void Validate(IEnumerable<RegistryItem> registries, RegistryItem registryItem);
    }
}
