﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC.Validation.Interfaces
{
    interface IRegistryItemValidation : IValidation
    {
        void Validate(RegistryItem registryItem);
    }
}
