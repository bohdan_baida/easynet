﻿using IOC.Interfaces;
using System.Collections.Generic;

namespace IOC.Validation.Interfaces
{
    interface IRegistryValidator : IValidation
    {
        void Validate(IEnumerable<RegistryItem> registry, RegistryItem newItem);
    }
}
