﻿using IOC.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IOC.Validation.Validations
{
    class LoopReferencesValidation : IRegistryValidation
    {
        public void Validate(IEnumerable<RegistryItem> registries, RegistryItem registryItem)
        {
            var newRegistryParameters = GetAllParameters(registryItem.ImplementationType, registries);

            foreach (var oldRegistry in registries)
            {
                var oldRegistryParameters = GetAllParameters(oldRegistry.ImplementationType, registries);
                if (oldRegistryParameters.Any(p => p.ParameterType.GUID == registryItem.AbstractionType.GUID)
                    && newRegistryParameters.Any(p => p.ParameterType.GUID == oldRegistry.AbstractionType.GUID))
                {
                    throw new InvalidOperationException($"Cycle references for type {registryItem.AbstractionType.Name} not allowed!");

                }
            }
        }

        public IEnumerable<ParameterInfo> GetAllParameters(Type type, IEnumerable<RegistryItem> registries)
        {
            var result = new List<ParameterInfo>();

            var parameters = type
                .GetConstructors()
                .SelectMany(p => p.GetParameters()
                .Where(x => registries.Select(c => c.ImplementationType.GUID).Contains(x.ParameterType.GUID)));

            result.AddRange(parameters);
            foreach (var parameter in parameters)
                result.AddRange(
                    GetAllParameters(registries.First(r => r.AbstractionType.GUID == parameter.ParameterType.GUID).ImplementationType, registries)
                );

            return result;
        }
    }
}
