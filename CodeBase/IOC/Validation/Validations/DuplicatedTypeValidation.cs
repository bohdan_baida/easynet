﻿using IOC.Interfaces;
using IOC.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC.Validation.Validations
{
    class DuplicatedTypeValidation : IRegistryValidation
    {
        public void Validate(IEnumerable<RegistryItem> registries, RegistryItem registryItem)
        {
            if (registries.Any(c => c.AbstractionType == registryItem.AbstractionType))
                throw new InvalidOperationException($"You cannot add abastraction {registryItem.AbstractionType.Name} twice!");
        }
    }
}
