﻿using IOC.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC.Validation.Validations
{
    public class AssignableValidation : IRegistryItemValidation
    {
        public void Validate(RegistryItem registryItem)
        {
            if (!registryItem.AbstractionType.IsAssignableFrom(registryItem.ImplementationType))
                throw new InvalidOperationException($"Type {registryItem.ImplementationType.Name} is not assignable from {registryItem.AbstractionType.Name}!");
        }
    }
}
