﻿using IOC.Interfaces;
using IOC.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace IOC.Validation
{
    public class RegistryValidator : IRegistryValidator
    {
        IEnumerable<IValidation> validations;
        public RegistryValidator()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var types = assembly.GetTypes();
            validations = types
                .Where(c => c.GetInterfaces().Any(i => i.GUID == typeof(IValidation).GUID)
                    && c.Namespace == "IOC.Validation.Validations")
                .Select(c => (IValidation)Activator.CreateInstance(c));

        }
        public void Validate(IEnumerable<RegistryItem> registries, RegistryItem newItem)
        {
            foreach (var registryItemValidation in validations.OfType<IRegistryItemValidation>())
                registryItemValidation.Validate(newItem);
            foreach (var registryValidation in validations.OfType<IRegistryValidation>())
                registryValidation.Validate(registries, newItem);
        }
    }
}
