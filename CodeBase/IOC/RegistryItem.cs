﻿using IOC.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    public class RegistryItem
    {
        public ObjectLifetimesEnum Lifetime { get; set; }

        public Type AbstractionType { get; set; }

        public Type ImplementationType { get; set; }

        public Dictionary<string, object> ConstructorParameters { get; set; }

        public object Instance { get; set; }

        public RegistryItem()
        {
            ConstructorParameters = new Dictionary<string, object>();
        }
    }
}
