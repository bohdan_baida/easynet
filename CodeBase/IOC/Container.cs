﻿using IOC.Enums;
using IOC.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace IOC
{
    public class Container : Interfaces.IContainer
    {
        ITypeResolver resolver;

        public Container(ITypeResolver resolver)
        {
            this.resolver = resolver;
        }

        public TAbstraction Resolve<TAbstraction>()
        {
            return (TAbstraction)resolver.ResolveType(typeof(TAbstraction));
        }
    }
}
