﻿using IOC.Enums;
using IOC.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    class TypeResolver : ITypeResolver
    {
        IRegistry configuration;

        public IRegistry Configuration => configuration;

        public TypeResolver(IRegistry configuration)
        {
            this.configuration = configuration;
        }

        public object ResolveType(Type type)
        {
            var registry = configuration.GetRegistry(type);
            if (registry == null)
                throw new InvalidOperationException($"The type {type.Name} is not registered");

            switch (registry.Lifetime)
            {
                case ObjectLifetimesEnum.Trancient:
                    return ResolveType(registry);
                case ObjectLifetimesEnum.Singleton:
                    return (registry.Instance == null
                                                ? registry.Instance = ResolveType(registry)
                                                : registry.Instance);
                default:
                    throw new InvalidEnumArgumentException(nameof(registry.Lifetime), (int)registry.Lifetime, typeof(ObjectLifetimesEnum));
            }
        }

        private object ResolveType(RegistryItem registry, List<Type> chain = null)
        {
            var localChain = GetValidatedChain(registry, chain);

            var constructor = FindBestMatchConstructor(registry);
            var parameters = new List<object>();
            foreach (var parameter in constructor.GetParameters())
            {
                if (registry.ConstructorParameters.Any(c => c.Key == parameter.Name))
                    parameters.Add(registry.ConstructorParameters.First(c => c.Key == parameter.Name).Value);
                else
                    parameters.Add(ResolveType(configuration.GetRegistry(parameter.ParameterType), localChain));

            }
            return constructor.Invoke(parameters.ToArray());
        }

        private ConstructorInfo FindBestMatchConstructor(RegistryItem registry)
        {
            Dictionary<ConstructorInfo, int> suitableParamsCount = new Dictionary<ConstructorInfo, int>();
            foreach (var constructor in registry.ImplementationType.GetConstructors())
            {
                var parameters = constructor.GetParameters();
                if (parameters.All(p => registry.ConstructorParameters.ContainsKey(p.Name) || configuration.ContainsRegistry(p.ParameterType)))
                {
                    suitableParamsCount.Add(constructor, parameters.Count());
                }
            }
            var bestCtor = suitableParamsCount.OrderByDescending(c => c.Value).FirstOrDefault();

            if (bestCtor.Key == null)
                throw new InvalidOperationException($"Can not resolve type {registry.AbstractionType.Name}");

            return bestCtor.Key;
        }

        private List<Type> GetValidatedChain(RegistryItem registry, List<Type> chain)
        {
            if (chain == null)
                chain = new List<Type>();

            var localChain = chain.ToList();

            if (localChain.Any(c => c == registry.AbstractionType))
                throw new InvalidOperationException($"Loop reference for type {registry.AbstractionType.Name}");

            localChain.Add(registry.AbstractionType);

            return localChain;
        }
    }
}
