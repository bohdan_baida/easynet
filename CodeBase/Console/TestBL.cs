﻿using System;
using System.Collections.Generic;

namespace Console
{
    public class TestBL
    {
        //Interceptor inter = new Interceptor();

        //[FacadeMethod]
        public virtual void A()
        {
            System.Console.WriteLine("a");
        }

        public virtual int B()
        {
            return 3 * 3;
        }

        public virtual int C(int a)
        {
            return a * 100;
        }

        //[FacadeMethod]
        public virtual List<string> D(int a)
        {
            var result = new List<string>();
            for (int i = 1; i <= a; i++)
            {
                result.Add(i.ToString());
            }
            System.Console.WriteLine("In method");
            return result;
        }

        public virtual int D(int a, int b)
        {
            return( a + b);
        }

        //public void XX(int a, int b)
        //{
        //    object[] vs;
        //    vs = new object[1];
        //    vs[0] = 1;
        //    inter.Intercept(vs);

        //}

    }
}
