using Fakes.Arguments;
using Fakes.TypeGeneration;
using System;
using System.Collections.Generic;
using UnitTesting.Generators;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //IRegistry registry = new Registry();
            //registry.AddTrancient<IA, A>();
            //registry.AddTrancient<IB, B>()
            //    .Ctor("x", "bla bla");
            //registry.AddTrancient<IC, C>()
            //    .Ctor("a", 12);


            //var container = new Container(registry.GetResolver());

            //var instanse = container.Resolve<IA>();
            //var instanse1 = container.Resolve<IC>();

            //var generator = new TypeGenerator();
            //var instance = generator.GetInstance<C>();

            //CreditFacade facade = FacadeFactory.Instance
            //    .GetFacade<CreditFacade>();

            //var methodBodyReader = new MethodILProvider(facade.GetType().GetMethod("XX"));
            //var codes = methodBodyReader.Format(true);
            //System.Console.WriteLine(codes);

            //var creditFacade = new TestBL();
            //var fake = new Fake<TestBL>();
            //fake.Setup(c => c.D(It.IsInRange(1,3), It.IsAny<int>()))
            //    .Returns(64);
            //fake.Setup(c => c.D(It.IsInRange(1, 4)))
            //    .Returns(new List<string> { "a", "b" });
            //var obj = fake.Object;

            //var result1 = creditFacade.D(1, 3);
            //var result = obj.D(1, 3);
            //var xx = obj.C(1);


            //var ifake = new Fake<ITest>();
            //ifake.Setup(c => c.Test1())
            //    .Returns(10);
            //var io = ifake.Object;
            //var ir  = io.Test1();
            //io.Test2(1);

            //var methodBodyReader = new MethodILProvider(obj.GetType().GetMethod("D", new Type[] { typeof(int), typeof(int) }));
            //var codes = methodBodyReader.Format(true);
            //System.Console.WriteLine(codes);
            //facade.A();
            ////var x = facade.B();
            //var result = facade.C(100);
            ////System.Console.WriteLine(x);
            ////System.Console.WriteLine(result);
            ////var result = facade.D(10, 12);
            ////System.Console.WriteLine(result);

            //var result1 = facade.D(10);
            //result1.ForEach(c => System.Console.WriteLine(c));

            var order = GeneratorFactory.Create<Order>()
                .Set(c => c.OrderDate = DateTime.Now)
                .Add<User>((o, u) =>
                {
                    o.User = u;
                    u.Name = "Bohdan";
                })
                .Add<OrderItem>((o, oi) =>
                {
                    o.OrderItems.Add(oi);
                    oi.GoodName = "Beer";
                });

            System.Console.Read();
        }
    }

    public class Order
    {
        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        public User User { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }
    }

    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class OrderItem
    {
        public int Id { get; set; }
        public string GoodName { get; set; }
    }

    public interface ITest
    {
        int Test1();
        void Test2(int a);
    }
    public interface IA
    {
    }

    class A : IA
    {
        IB c;
        public A(IB c)
        {
            this.c = c;
        }
    }

    interface IB
    {
    }

    class B : IB
    {
        IC a;
        string x;
        public B(IC a, string x)
        {
            this.a = a;
            this.x = x;
        }
    }

    interface IC
    {
    }

    public class C : IC
    {
        int a;

        public int A => a;
        public C(int a)
        {
            this.a = a;
        }

        public C()
        {
            a = 10;
        }
    }


}
