﻿using Fakes.MemberSetup;
using Fakes.MemberSetup.Abstract;
using System;
using System.Linq.Expressions;

namespace Fakes.TypeGeneration
{
    public class Fake<T>
        where T : class
    {
        IMembersSetup membersSetup;

        public Fake(IMembersSetup memberSetup)
        {
        }
        public Fake()
        {
            membersSetup = new MembersSetup();
        }
        public T Object
        {
            get
            {
                var typeGenerator = new TypeGenerator(membersSetup);
                return typeGenerator.GetInstance<T>();
            }
        }

        public IMemberSetup<T, R> Setup<R>(Expression<Func<T, R>> expression)
        {
            var method = (MethodCallExpression)expression.Body;

            var memberSetup = new MemberSetup<T, R>(expression);
            membersSetup.Add(memberSetup.MemberSetupInfo);

            return memberSetup;
        }



    }
}
