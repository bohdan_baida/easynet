﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Verification
{
    public class MemberCallHistoryItem
    {
        public string TypeName { get; set; }

        public string MethodName { get; set; }

        public object[] Parameters { get; set; }

        public int Times { get; set; }
    }
}
