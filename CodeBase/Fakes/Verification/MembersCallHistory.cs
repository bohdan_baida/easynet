﻿using Fakes.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Verification
{
    public class MembersCallHistory
    {
        List<MemberCallHistoryItem> methodCallHistoryItems;

        public MembersCallHistory()
        {
            methodCallHistoryItems = new List<MemberCallHistoryItem>();
        }

        public void Handle(string memberName, string typeName, object[] parameters)
        {
            var existingExecution = Find(memberName, typeName, parameters);
            if (existingExecution == null)
            {
                AddHistory(memberName, typeName, parameters);
            }
            else
            {
                existingExecution.Times++;
            }
        }
        private void AddHistory(string memberName, string typeName, object[] parameters)
        {
            methodCallHistoryItems.Add(new MemberCallHistoryItem()
            {
                MethodName = memberName,
                TypeName = typeName,
                Parameters = parameters,
                Times = 1
            });
        }

        private MemberCallHistoryItem Find(string memberName, string typeName, object[] parameters)
        {
            return methodCallHistoryItems.FirstOrDefault(c => c.TypeName == typeName
                && c.MethodName == memberName
                && IsParametersTheSame(c.Parameters, parameters));
        }

        private bool IsParametersTheSame(object[] originalParameters, params object[] parameters)
        {
            if (originalParameters.Count() != parameters.Count())
                return false;

            for (int i = 0; i < originalParameters.Count(); i++)
            {
                if (originalParameters[i] != parameters[i])
                {
                    return false;
                }
            }

            return true;
        }

    }
}
