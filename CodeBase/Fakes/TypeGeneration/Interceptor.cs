﻿using Fakes.Arguments;
using Fakes.MemberSetup;
using Fakes.MemberSetup.Abstract;
using Fakes.Verification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.TypeGeneration
{
    public class Interceptor
    {
        private IMembersSetup membersSetup;
        private MembersCallHistory methodsCallHistory;

        public Interceptor(IMembersSetup membersSetup)
        {
            this.membersSetup = membersSetup;
            this.methodsCallHistory = new MembersCallHistory();
        }

        public object Intercept(string className, string memberName, object[] parameters)
        {
            MemberSetupInfo memberSetup;

            if ((memberSetup = membersSetup.Find(className, memberName, parameters)) != null)
            {
                SaveExecutionToHistory(className, memberName, parameters);

                return memberSetup.Result;
            }
            return null;
        }

        public void InterceptUnsetuped(string className, string memberName, object[] parameters)
        {
            SaveExecutionToHistory(className, memberName, parameters);
        }

        public void SaveExecutionToHistory(string className, string memberName, params object[] parameters)
        {
            methodsCallHistory.Handle(memberName, className, parameters);
        }
    }
}
