﻿using Fakes.MemberSetup;
using Fakes.MemberSetup.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fakes.TypeGeneration
{
    public class TypeGenerator
    {
        private static readonly AssemblyBuilder assemblyBuilder;
        private static readonly ModuleBuilder moduleBuilder;

        private TypeBuilder typeBuilder;
        private FieldBuilder interceptorFieldBuilder;

        private IMembersSetup membersSetup;
        static TypeGenerator()
        {
            assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
                new AssemblyName("FakeAssembly"),
                AssemblyBuilderAccess.Run);
            moduleBuilder = assemblyBuilder.DefineDynamicModule("FakeModule");
        }

        public TypeGenerator(IMembersSetup membersSetup)
        {
            this.membersSetup = membersSetup;
        }

        public T GetInstance<T>()
        {
            return (T)Activator.CreateInstance(GenerateType(typeof(T)), new Interceptor(membersSetup));
        }

        public Type GenerateType(Type baseType)
        {
            var guid = Guid.NewGuid();
            typeBuilder = moduleBuilder.DefineType(
                $"{baseType.Name}_Proxy_{guid}",
                TypeAttributes.Public);

            if (baseType.IsInterface)
            {
                typeBuilder.AddInterfaceImplementation(baseType);
                DefineConstructor(baseType, true);
            }
            else
            {
                typeBuilder.SetParent(baseType);
                DefineConstructor(baseType, false);
            }

            DefineMembers();
            DefineUnsettupedMembers(baseType);

            return typeBuilder.CreateType();
        }

        public void DefineConstructor(Type baseType, bool isInterface)
        {
            interceptorFieldBuilder = typeBuilder.DefineField(
                "interceptor",
                typeof(Interceptor),
                FieldAttributes.Private);
            var ctorBuilder = typeBuilder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard, new Type[] { typeof(Interceptor) });
            ILGenerator il = ctorBuilder.GetILGenerator();
            il.Emit(OpCodes.Ldarg_0);
            if (!isInterface)
            {
                var constructor = baseType.GetConstructors()
                    .First(c => c.GetParameters().Count() == 0);
                il.Emit(OpCodes.Call, constructor);
                il.Emit(OpCodes.Ldarg_0);
            }
            il.Emit(OpCodes.Ldarg_1);
            il.Emit(OpCodes.Stfld, interceptorFieldBuilder);
            il.Emit(OpCodes.Ret);
        }


        private void DefineMembers()
        {
            var interceptMethod = interceptorFieldBuilder.FieldType.GetMethod("Intercept");
            foreach (var memberSetup in membersSetup)
            {
                switch (memberSetup.Member.MemberType)
                {
                    case MemberTypes.Method:
                        DefineMockedMethod(memberSetup, interceptMethod);
                        break;
                    case MemberTypes.Property:
                        throw new NotImplementedException();
                    default:
                        throw new ArgumentException("Following member type is noy supported");
                }
            }
        }

        private void DefineUnsettupedMembers(Type type)
        {
            var interceptMethod = interceptorFieldBuilder.FieldType.GetMethod("InterceptUnsetuped");
            var members = type.GetMembers()
                .Where(c => c.MemberType == MemberTypes.Method || c.MemberType == MemberTypes.Property)
                .Except(membersSetup.Select(c => c.Member).ToList());
            foreach (var method in members.OfType<MethodInfo>().Where(c => c.IsVirtual || c.IsAbstract))
            {
                DefineMethod(method, interceptMethod);
            }
        }

        private void DefineMockedMethod(MemberSetupInfo memberSetup, MethodInfo interceptMethod)
        {
            var method = (MethodInfo)memberSetup.Member;
            var param = method.GetParameters()
                   .Select(c => c.ParameterType)
                   .ToArray();
            var methodBuilder = typeBuilder.DefineMethod(
                method.Name,
                MethodAttributes.Virtual | MethodAttributes.Public,
                method.ReturnType,
                param);

            var il = methodBuilder.GetILGenerator();
            il.DeclareLocal(typeof(object[]));
            il.DeclareLocal(typeof(object));

            il.Emit(OpCodes.Nop);
            il.Emit(OpCodes.Ldc_I4, param.Length);
            il.Emit(OpCodes.Newarr, typeof(object));
            il.Emit(OpCodes.Stloc_0);

            for (int i = 1; i <= param.Length; i++)
            {
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Ldc_I4, i - 1);
                il.Emit(OpCodes.Ldarg, i);
                if (!param[i - 1].IsClass)
                {
                    il.Emit(OpCodes.Box, param[i - 1]);
                }
                il.Emit(OpCodes.Stelem_Ref);
            }

            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldfld, interceptorFieldBuilder);
            il.Emit(OpCodes.Ldstr, method.DeclaringType.FullName);
            il.Emit(OpCodes.Ldstr, method.ToString());
            il.Emit(OpCodes.Ldloc_0);
            il.Emit(OpCodes.Callvirt, interceptMethod);

            if (!method.ReturnType.IsClass)
            {
                il.Emit(OpCodes.Unbox_Any, method.ReturnType);
            }
            il.Emit(OpCodes.Ret);

            typeBuilder.DefineMethodOverride(methodBuilder, method);
        }


        private void DefineMethod(MethodInfo method, MethodInfo interceptMethod)
        {
            var param = method.GetParameters()
                   .Select(c => c.ParameterType)
                   .ToArray();
            var methodBuilder = typeBuilder.DefineMethod(
                method.Name,
                MethodAttributes.Virtual | MethodAttributes.Public,
                method.ReturnType,
                param);

            var il = methodBuilder.GetILGenerator();
            il.DeclareLocal(typeof(object[]));
            il.Emit(OpCodes.Nop);
            il.Emit(OpCodes.Ldc_I4, param.Length);
            il.Emit(OpCodes.Newarr, typeof(object));
            il.Emit(OpCodes.Stloc_0);

            for (int i = 1; i <= param.Length; i++)
            {
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Ldc_I4, i - 1);
                il.Emit(OpCodes.Ldarg, i);
                if (!param[i - 1].IsClass)
                {
                    il.Emit(OpCodes.Box, param[i - 1]);
                }
                il.Emit(OpCodes.Stelem_Ref);
            }

            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldfld, interceptorFieldBuilder);
            il.Emit(OpCodes.Ldstr, method.DeclaringType.FullName);
            il.Emit(OpCodes.Ldstr, method.ToString());
            il.Emit(OpCodes.Ldloc_0);
            il.Emit(OpCodes.Callvirt, interceptMethod);
            if (method.IsAbstract == false)
            {
                il.Emit(OpCodes.Ldarg_0);
                for (int i = 1; i <= param.Length; i++)
                {
                    il.Emit(OpCodes.Ldarg, i);
                }
                il.Emit(OpCodes.Call, method);
            }
            il.Emit(OpCodes.Ret);

            typeBuilder.DefineMethodOverride(methodBuilder, method);
        }
    }
}
