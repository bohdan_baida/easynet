﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.POC
{
    public class FacadeFactory
    {
        private static FacadeFactory facadeFactory;

        public static FacadeFactory Instance => facadeFactory == null 
            ? facadeFactory = new FacadeFactory() 
            : facadeFactory;

        private FacadeFactory()
        {

        }

        public TFacade GetFacade<TFacade>()
        {
            return new FacadeGenerator().Get<TFacade>();
        }

    }
}
