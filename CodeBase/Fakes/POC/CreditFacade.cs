﻿using Fakes.POC.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.POC
{
    public class CreditFacade
    {
        [FacadeMethod]
        public virtual void A()
        {
            Console.WriteLine("a");
        }

        public virtual int B()
        {
            return 3 * 3;
        }

        public virtual int C(int a)
        {
            return a * 100;
        }

        [FacadeMethod]
        public virtual List<string> D(int a)
        {
            var result = new List<string>();
            for (int i = 1; i <= a; i++)
            {
                result.Add(i.ToString());
            }
            Console.WriteLine("In method");
            return result;
        }

        public virtual int D(int a, int b)
        {
            return a + b;
        }

    }
}
