﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.POC
{
    public class Logger
    {
        public void LogExecution(string message)
        {
            Console.WriteLine(message);
        }
    }
}
