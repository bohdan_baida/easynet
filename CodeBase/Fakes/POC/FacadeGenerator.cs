﻿using Fakes.POC.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fakes.POC
{
    public class FacadeGenerator
    {
        private static readonly AssemblyBuilder assemblyBuilder;
        private static readonly ModuleBuilder moduleBuilder;
        private FieldBuilder field;

        private TypeBuilder typeBuilder;

        static FacadeGenerator()
        {
            assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
                new AssemblyName("FakeAssembly"),
                AssemblyBuilderAccess.Run);
            moduleBuilder = assemblyBuilder.DefineDynamicModule("FakeModule");
        }

        public FacadeGenerator()
        {

        }

        public T Get<T>()
        {
            return (T)Activator.CreateInstance(GenerateType(typeof(T)));
        }

        public Type GenerateType(Type baseType)
        {
            var guid = Guid.NewGuid();
            typeBuilder = moduleBuilder.DefineType(
                $"{baseType.Name}_Proxy_{guid}",
                TypeAttributes.Public);

            typeBuilder.SetParent(baseType);
            DefineConstructor(baseType);
            CreateMethods(baseType);
            return typeBuilder.CreateType();
        }

        public void CreateMethods(Type baseType)
        {
            var methods = baseType.GetMethods().Where(m=>m.IsVirtual && m.GetCustomAttribute(typeof(FacadeMethodAttribute)) != null);
            var logMethod = field.FieldType.GetMethod("LogExecution");
            foreach (var method in methods)
            {
                var param = method.GetParameters()
                    .Select(c => c.ParameterType)
                    .ToArray();
                var methodBuilder = typeBuilder.DefineMethod(
                    method.Name,
                    MethodAttributes.Virtual | MethodAttributes.Public,
                    method.ReturnType,
                    param);

                var il = methodBuilder.GetILGenerator();
                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldstr, "Begin of execution");
                il.Emit(OpCodes.Call, logMethod);

                il.Emit(OpCodes.Ldarg_0);
                for (int i = 1; i <= param.Length; i++)
                {
                    il.Emit(OpCodes.Ldarg, i);
                }
                il.Emit(OpCodes.Call, method);

                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldstr, "End of execution");
                il.Emit(OpCodes.Call, logMethod);

                il.Emit(OpCodes.Ret);

                typeBuilder.DefineMethodOverride(methodBuilder, method);
            }
        }

        public void DefineConstructor(Type baseType)
        {
            var constructor = baseType.GetConstructors()
                .First(c => c.GetParameters().Count() == 0);

            field = typeBuilder.DefineField(
                "logger",
                typeof(Logger),
                FieldAttributes.Private);
            var loggerCtor = typeof(Logger).GetConstructors()
                .FirstOrDefault(c => c.GetParameters().Count() == 0);

            var ctorBuilder = typeBuilder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard, new Type[] { });
            ILGenerator il = ctorBuilder.GetILGenerator();
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Call, constructor);
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Newobj, loggerCtor);
            il.Emit(OpCodes.Stfld, field);
            il.Emit(OpCodes.Ret);
        }
    }
}
