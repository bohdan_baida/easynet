﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace FSA.IL
{
    public class MethodILProvider
    {
        private readonly byte[] codes;
        private readonly MethodInfo method;
        private readonly Module module;

        public IEnumerable<byte> Codes => this.codes.AsEnumerable();
        public List<ILInstruction> Instructions { get; set; }

        /// <summary>MethodBodyReader constructor</summary>
        /// <param name="method">The System.Reflection defined MethodInfo</param>
        public MethodILProvider(MethodInfo method)
        {
            this.method = method;
            this.module = method.Module;

            try
            {
                var body = method.GetMethodBody();
                if (body == null)
                    return;

                this.codes = body.GetILAsByteArray();
            }
            catch
            {
                var owner = method.GetType().GetField("m_owner", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(method);
                var resolver = owner.GetType().GetField("m_resolver", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(owner);
                if (resolver == null)
                    throw new ArgumentException("The dynamic method's IL has not been finalized.");

                this.codes = (byte[])resolver.GetType().GetField("m_code", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(resolver);
            }


            this.Instructions = this.GetInstructions().ToList();
        }

        public IEnumerable<byte> GetCodesFromInstruction() => this.Instructions.SelectMany(x => x.GetBytes());
        /// <summary>Gets the IL code of the method</summary>
        public string Format(bool includeTokens = false) => includeTokens
            ? this.GetBodyCode(this.GetCodesFromInstruction().ToArray())
            : this.GetBodyCode();

        private string GetBodyCode()
        {
            var resultBuilder = new StringBuilder();
            foreach (var instruction in this.Instructions)
                resultBuilder.Append($"{instruction.GetCode()}\n");

            return resultBuilder.ToString();
        }
        ///// <summary>Gets the IL code of the method</summary>
        private string GetBodyCode(byte[] codes)
        {
            if (this.Instructions.Count == 0)
                return string.Empty;

            var resultBuilder = new StringBuilder();
            for (var index = 0; index < this.Instructions.Count; index++)
            {
                var instruction = this.Instructions[index];
                var length = index == this.Instructions.Count - 1
                    ? codes.Length - instruction.Offset - 1
                    : this.Instructions[index + 1].Offset - instruction.Offset - 1;

                var bytes = this.ReadBytes(codes, instruction.Offset + 1, length);
                var lineBuilder = new StringBuilder($"  ({codes[instruction.Offset]}");
                foreach (var operandCode in bytes)
                    lineBuilder.Append($"|{operandCode.ToString()}");

                lineBuilder.Append($") {(length == 4 ? this.ToInt32(bytes).ToString() : string.Empty)}");

                resultBuilder.Append($"{this.Instructions[index].GetCode()}{lineBuilder.ToString()}\n");
            }

            return resultBuilder.ToString();
        }
        /// <summary>Constructs the array of ILInstructions according to the IL byte code.</summary>
        private IEnumerable<ILInstruction> GetInstructions()
        {
            var position = 0;
            var index = -1;
            while (position < this.codes.Length)
            {
                index++;
                var code = (ushort)this.codes[position++];
                var opCode = code != 0xfe
                    ? OpCodesProvider.GetSingleByte(code)
                    : OpCodesProvider.GetMultiByte(this.codes[position++]);

                yield return this.GetInstruction(opCode, index, ref position);
            }
        }
        private ILInstruction GetInstruction(OpCode opCode, int index, ref int position)
        {
            try
            {
                var result = new ILInstruction(opCode, index, position - 1);
                switch (opCode.OperandType)
                {
                    case OperandType.ShortInlineBrTarget:
                        return result
.SetOperand(this.ReadSByte(ref position) + position);
                    case OperandType.ShortInlineI:
                        return result
 .SetOperand(this.ReadSByte(ref position));
                    case OperandType.ShortInlineR:
                        return result
 .SetOperand(this.ReadSingle(ref position));
                    case OperandType.ShortInlineVar:
                        return result
.SetOperand(this.ReadByte(ref position));
                    case OperandType.InlineBrTarget:
                        return result
.SetOperand(this.ReadInt32(ref position));
                    case OperandType.InlineField:
                        return result
  .SetOperand(this.module?.ResolveField(this.ReadInt32(ref position)));
                    case OperandType.InlineSig:
                        return result
    .SetOperand(this.module?.ResolveSignature(this.ReadInt32(ref position)));
                    case OperandType.InlineVar:
                        return result
    .SetOperand(this.ReadUInt16(ref position));
                    case OperandType.InlineI:
                        return result
      .SetOperand(this.ReadInt32(ref position));
                    case OperandType.InlineI8:
                        return result
     .SetOperand(this.ReadInt64(ref position));
                    case OperandType.InlineNone: return result;
                    case OperandType.InlineR:
                        return result
      .SetOperand(this.ReadDouble(ref position));
                    case OperandType.InlineMethod:
                        return this.module == null ? result : result.SetToken(this.ReadInt32(ref position))
 .SetOperand(this.TryResolveMethod(result.Token) ?? this.TryResolveMember(result.Token));
                    case OperandType.InlineTok:
                        return result.SetToken(this.ReadInt32(ref position))
    .SetOperand(this.TryResolveType(result.Token));
                    case OperandType.InlineType:
                        return result.SetToken(this.ReadInt32(ref position))
   .SetOperand(this.TryResolveType(result.Token, this.method?.DeclaringType?.GetGenericArguments() ?? Type.EmptyTypes, GetGenericArguments(this.method)));
                    case OperandType.InlineString:
                        return result.SetToken(this.ReadInt32(ref position))
 .SetOperand(this.module.ResolveString(result.Token) + result.Token.ToString());
                    case OperandType.InlineSwitch:
                        result.SetToken(this.ReadInt32(ref position));

                        var casesAddresses = new int[result.Token];
                        for (var i = 0; i < result.Token; i++)
                            casesAddresses[i] = this.ReadInt32(ref position);

                        var cases = new int[result.Token];
                        for (var i = 0; i < result.Token; i++)
                            cases[i] = position + casesAddresses[i];

                        return result;
                    default:
                        throw new Exception("Unknown operand type.");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private int ReadInt16(ref int position)
            => ((this.codes[position++] | (this.codes[position++] << 8)));
        private ushort ReadUInt16(ref int position)
            => (ushort)((this.codes[position++] | (this.codes[position++] << 8)));
        private int ReadInt32(ref int position)
            => ((this.codes[position++] | (this.codes[position++] << 8)) | (this.codes[position++] << 0x10)) | (this.codes[position++] << 0x18);
        private ulong ReadInt64(ref int position)
            => (ulong)(((this.codes[position++] | (this.codes[position++] << 8)) | (this.codes[position++] << 0x10)) | (this.codes[position++] << 0x18) | (this.codes[position++] << 0x20) | (this.codes[position++] << 0x28) | (this.codes[position++] << 0x30) | (this.codes[position++] << 0x38));
        private double ReadDouble(ref int position)
            => (((this.codes[position++] | (this.codes[position++] << 8)) | (this.codes[position++] << 0x10)) | (this.codes[position++] << 0x18) | (this.codes[position++] << 0x20) | (this.codes[position++] << 0x28) | (this.codes[position++] << 0x30) | (this.codes[position++] << 0x38));
        private sbyte ReadSByte(ref int position)
            => (sbyte)this.codes[position++];
        private byte ReadByte(ref int position)
            => this.codes[position++];
        private Single ReadSingle(ref int position)
            => (((this.codes[position++] | (this.codes[position++] << 8)) | (this.codes[position++] << 0x10)) | (this.codes[position++] << 0x18));

        private Type[] GetGenericArguments(MethodInfo method)
        {
            try
            {
                return method.GetGenericArguments();
            }
            catch
            {
                return Type.EmptyTypes;
            }
        }
        private object TryResolveType(int token, Type[] typeGenericParameters, Type[] methodGenericParagemeters, object defaultValue = null)
            => this.Resolve(token, typeGenericParameters, methodGenericParagemeters, this.module.ResolveType) ?? defaultValue;
        private object TryResolveType(int token, object defaultValue = null)
            => this.Resolve(token, this.module.ResolveType) ?? defaultValue;

        private MethodBase TryResolveMethod(int token, MethodBase defaultValue = null)
            => this.Resolve(token, this.module.ResolveMethod) ?? defaultValue;
        private MemberInfo TryResolveMember(int token, MemberInfo defaultValue = null)
            => this.Resolve(token, this.module.ResolveMember) ?? defaultValue;
        private string TryResolveString(int token, string defaultValue = null)
            => this.Resolve(token, this.module.ResolveString) ?? defaultValue;

        private T Resolve<T>(int token, Type[] typeGenericParameters, Type[] methodGenericParagemeters, Func<int, T> resolver)
        {
            try
            {
                return resolver(token);
            }
            catch { return default(T); }
        }
        private T Resolve<T>(int token, Func<int, T> resolver)
        {
            try
            {
                return resolver(token);
            }
            catch { return default(T); }
        }

        private IEnumerable<byte> ReadBytes(byte[] codes, int position, int length)
        {
            for (var index = position; index < position + length; index++)
                yield return codes[index];
        }
        private int ToInt32(IEnumerable<byte> codes)
        {
            var index = 0;

            var result = 0;
            foreach (var code in codes)
                result |= (code << 8 * (index++));

            return result;
        }
    }
}