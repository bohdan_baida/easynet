﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;

namespace FSA.IL
{
    public class ILInstruction
    {
        public int Index { get; }
        public OpCode OpCode { get; }
        public MemberTypes MemberType { get; private set; }
        public object Operand { get; set; }
        public int Offset { get; }
        public int Token { get; set; }

        public ILInstruction(OpCode opCode, int index, int offset)
        {
            this.Index = index;
            this.Token = -1;
            this.OpCode = opCode;
            this.Offset = offset;
        }

        public byte[] GetBytes()
        {
            byte[] result;
            if (this.Token != -1)
            {
                var tokenBytes = ILManager.ToBytes(this.Token).ToArray();

                result = new byte[tokenBytes.Count() + 1];

                result[0] = (byte)this.OpCode.Value;
                tokenBytes.CopyTo(result, 1);
            }
            else if (this.Operand != null)
            {
                var operandBytes = this.OpCode.OperandType == OperandType.InlineBrTarget || this.OpCode.OperandType == OperandType.ShortInlineBrTarget
                    ? this.ToBytes((int)this.Operand).ToArray()
                    : this.ToBytes(this.Operand);

                result = new byte[operandBytes.Count() + 1];

                result[0] = (byte)this.OpCode.Value;
                operandBytes.CopyTo(result, 1);
            }
            else
                return new byte[] { (byte)this.OpCode.Value };

            return result;
        }
        /// <summary>Returns a friendly strign representation of this instruction</summary>
        public string GetCode()
        {
            var result = $"{GetExpandedOffset(this.Offset)} : {this.OpCode}";
            if (this.Operand == null)
                return result;

            switch (this.OpCode.OperandType)
            {
                case OperandType.InlineField:
                    var field = (FieldInfo)this.Operand;
                    result += $" {this.ProcessSpecialTypes(field.FieldType.ToString())} {this.ProcessSpecialTypes(field.ReflectedType.ToString())}::{field.Name}";

                    break;
                case OperandType.InlineMethod:
                    try
                    {
                        var method = (MethodInfo)this.Operand;
                        if (!method.IsStatic)
                            result += " instance";

                        result += $" {this.ProcessSpecialTypes(method.ReturnType.ToString())} {this.ProcessSpecialTypes(method.ReflectedType.ToString())}::{method.Name}()";
                    }
                    catch
                    {
                        try
                        {
                            var ctor = (ConstructorInfo)this.Operand;
                            if (!ctor.IsStatic)
                                result += " instance";

                            result += $" void {this.ProcessSpecialTypes(ctor.ReflectedType.ToString())}::{ctor.Name}()";
                        }
                        catch { }
                    }
                    break;
                case OperandType.ShortInlineBrTarget:
                case OperandType.InlineBrTarget: result += $" {this.GetExpandedOffset((int)this.Operand)}"; break;
                case OperandType.InlineType: result += $" {this.ProcessSpecialTypes(this.Operand.ToString())}"; break;
                case OperandType.InlineString: result += this.Operand.ToString() == "\r\n" ? " \"\\r\\n\"" : $" \"{this.Operand.ToString()}\""; break;
                case OperandType.ShortInlineVar: result += this.Operand.ToString(); break;
                case OperandType.InlineI:
                case OperandType.InlineI8:
                case OperandType.InlineR:
                case OperandType.ShortInlineI:
                case OperandType.ShortInlineR: result += this.Operand.ToString(); break;
                case OperandType.InlineTok: result += this.Operand is Type type ? type.FullName : "not supported"; break;
                default:
                    result += "not supported"; break;
            }

            return result;
        }

        public ILInstruction SetOperand(FieldInfo field)
        {
            this.MemberType = MemberTypes.Field;
            this.Operand = field;

            return this;
        }
        public ILInstruction SetOperand(MethodBase method)
        {
            this.MemberType = MemberTypes.Method;
            this.Operand = method;

            return this;
        }
        public ILInstruction SetOperand(object operand)
        {
            this.Operand = operand;

            return this;
        }

        public ILInstruction SetToken(int token)
        {
            this.Token = token;

            return this;
        }

        /// <summary>Add enough zeros to a number as to be represented on 4 characters</summary>
        /// <param name="offset">The number that must be represented on 4 characters</param>
        private string GetExpandedOffset(long offset)
        {
            var resultBuilder = new StringBuilder(offset.ToString());
            for (var index = 0; resultBuilder.Length < 4; index++)
                resultBuilder.Insert(0, "0");

            return resultBuilder.ToString();
        }
        /// <summary>Retrieve the friendly name of a type</summary>
        /// <param name="typeName">The complete name to the type</param>
        /// <returns>The simplified name of the type (i.e. "int" instead f System.Int32)</returns>
        private string ProcessSpecialTypes(string typeName)
        {
            switch (typeName)
            {
                case "System.string":
                case "System.String":
                case "String": return "string";
                case "System.Int32":
                case "Int":
                case "Int32": return "int";
            }

            return typeName;
        }

        private IEnumerable<byte> ToBytes(int value)
        {
            yield return (byte)value;
            yield return (byte)(value >> 8);
            yield return (byte)(value >> 16);
            yield return (byte)(value >> 24);
        }

        public byte[] ToBytes(object obj)
        {
            if (obj == null)
                return default(byte[]);

            if (obj is IList list)
                return ToGuid(list.OfType<object>().ToArray()).ToByteArray();

            return ToBytes(obj, new BinaryFormatter());
        }

        public Guid ToGuid(params object[] objects)
        {
            //TODO: probably need firstly try to get bytes from serializable object
            var builder = new StringBuilder();
            foreach (var obj in objects)
                builder.Append(Encoding.UTF8.GetString(ToBytes(obj)));//result.Combine(obj.ToByteArray());

            return ToGuid(builder.ToString());
        }
        private byte[] ToBytes(object obj, IFormatter formater)
        {
            using (var stream = new MemoryStream())
                try
                {
                    formater.Serialize(stream, obj);
                    return stream.ToArray();
                }
                catch { return ToBytes(obj.ToString()); }
        }
    }
}

