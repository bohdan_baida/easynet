﻿using System;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;

//using FSA.IL.Enums;
using System.Linq;

namespace FSA.IL
{
    public static class ILManager
    {
        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern IntPtr LoadLibraryW(string lpFileName);
        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        private static extern bool FreeLibrary(IntPtr hModule);
        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        //[UnmanagedFunctionPointer(CallingConvention.StdCall)]
        //private delegate bool UpdateMethodBodyIL(IntPtr pMethodTable, IntPtr pMethodHandle, int md, IntPtr pBuffer, int dwSize);
        //[UnmanagedFunctionPointer(CallingConvention.StdCall)]
        //private delegate Status GetStatus();
        //[UnmanagedFunctionPointer(CallingConvention.StdCall)]
        //private delegate Status WaitForIntialize();

        //private static IntPtr moduleHandle;
        //private static readonly UpdateMethodBodyIL updateILCodes;
        //private static readonly GetStatus getStatus;
        //private static readonly WaitForIntialize waitForIntialize;

        //public static Status Status { get; private set; } = Status.Uninitialized;

        static ILManager()
        {
            //var currentDirection = Regex.Replace(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), @"^(file\:\\)", string.Empty);
            //var path = Path.Combine(currentDirection, "Injection32.dll");

            //moduleHandle = LoadLibraryW(path);
            //if (moduleHandle == IntPtr.Zero)
            //    throw new FileNotFoundException(string.Format("Failed to load [{0}]", path));

            //var ptr = GetProcAddress(moduleHandle, "UpdateILCodes");
            //if (ptr == IntPtr.Zero)
            //    throw new MethodAccessException("Failed to locate UpdateILCodes function!");
            //updateILCodes = (UpdateMethodBodyIL)Marshal.GetDelegateForFunctionPointer(ptr, typeof(UpdateMethodBodyIL));

            //ptr = GetProcAddress(moduleHandle, "GetStatus");
            //if (ptr == IntPtr.Zero)
            //    throw new MethodAccessException("Failed to locate GetStatus function!");
            //getStatus = (GetStatus)Marshal.GetDelegateForFunctionPointer(ptr, typeof(GetStatus));

            //ptr = GetProcAddress(moduleHandle, "WaitForIntializationCompletion");
            //if (ptr == IntPtr.Zero)
            //    throw new MethodAccessException("Failed to locate WaitForIntializationCompletion function!");
            //waitForIntialize = (WaitForIntialize)Marshal.GetDelegateForFunctionPointer(ptr, typeof(WaitForIntialize));

            //Status = waitForIntialize();
        }

        public static string GetMethodCodes(MethodInfo method, bool includeTokens = false)
            => new MethodILProvider(method).Format(includeTokens);

        public static IEnumerable<byte> ToBytes(int value)
        {
            yield return (byte)value;
            yield return (byte)(value >> 8);
            yield return (byte)(value >> 16);
            yield return (byte)(value >> 24);
        }
        public static int ToInt32(IEnumerable<byte> codes)
        {
            var index = 0;

            var result = 0;
            foreach (var code in codes)
                result |= (code << 8 * (index++));

            return result;
        }

        //public static void ReplaceMethodBody(MethodBase method, MethodBase newMethod) => UpdateMethodBody(method, newMethod.GetMethodBody().GetILAsByteArray());
        //public static void UpdateMethodBody(MethodBase method, byte[] ilCodes)
        //{
        //    RuntimeHelpers.PrepareMethod(method.MethodHandle);

        //    if (updateILCodes == null)
        //        throw new Exception("Please Initialize() first.");

        //    //typePointer is pointer(address) to type in virtual memory
        //    var typePointer = IntPtr.Zero;
        //    if (method.DeclaringType != null)
        //        typePointer = method.DeclaringType.TypeHandle.Value;

        //    //methodPointer is pointer(address) to method in virtual memory
        //    var methodPointer = method is DynamicMethod
        //        ? GetDynamicMethodHandle(method)
        //        : method.MethodHandle.Value;

        //    var bufferPointer = Marshal.AllocHGlobal(ilCodes.Length);
        //    if (bufferPointer == IntPtr.Zero)
        //        throw new OutOfMemoryException();

        //    Marshal.Copy(ilCodes, 0, bufferPointer, ilCodes.Length);

        //    var token = 0;
        //    try
        //    {
        //        token = method.MetadataToken;
        //    }
        //    catch { }

        //    if (!updateILCodes(typePointer, methodPointer, token, bufferPointer, ilCodes.Length))
        //        throw new Exception("UpdateILCodes() failed, please check the initialization is failed or uncompleted.");
        //}

        //public static void Dispose()
        //{
        //    if (moduleHandle != IntPtr.Zero)
        //    {
        //        FreeLibrary(moduleHandle);
        //        moduleHandle = IntPtr.Zero;
        //    }
        //}

        private static IntPtr GetDynamicMethodHandle(MethodBase method)
        {
            const BindingFlags privateFlags = BindingFlags.Instance | BindingFlags.NonPublic;
            const BindingFlags publicFlags = BindingFlags.Instance | BindingFlags.Public;

            // .Net 4.0
            var fieldInfo = typeof(DynamicMethod).GetField("m_methodHandle", privateFlags);
            if (fieldInfo == null)
                return IntPtr.Zero;

            var runtimeMethodInfoStub = fieldInfo.GetValue(method);
            if (runtimeMethodInfoStub == null)
                return IntPtr.Zero;

            fieldInfo = runtimeMethodInfoStub.GetType().GetField("m_value", publicFlags);
            if (fieldInfo == null)
                return IntPtr.Zero;

            var internalRuntimeMethodHandle = fieldInfo.GetValue(runtimeMethodInfoStub);
            if (internalRuntimeMethodHandle == null)
                return IntPtr.Zero;

            fieldInfo = internalRuntimeMethodHandle.GetType().GetField("m_handle", privateFlags);
            if (fieldInfo == null)
                return IntPtr.Zero;

            return (IntPtr)fieldInfo.GetValue(internalRuntimeMethodHandle);

            // .Net 2.0
            //{
            //    FieldInfo fieldInfo = typeof(DynamicMethod).GetField("m_method", BindingFlags.NonPublic | BindingFlags.Instance);
            //    if (fieldInfo != null)
            //    {
            //        return ((RuntimeMethodHandle)fieldInfo.GetValue(method)).Value;
            //    }
            //}
        }
    }
}
