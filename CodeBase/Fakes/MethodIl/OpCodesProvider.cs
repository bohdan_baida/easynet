﻿using System;
using System.Linq;
using System.Reflection.Emit;

//using FSA.Tools.Reflection.Members.Fields;

namespace FSA.IL
{
    internal static class OpCodesProvider
    {
        private static readonly OpCode[] multiByteOpCodes;
        private static readonly OpCode[] singleByteOpCodes;

        static OpCodesProvider()
        {
            singleByteOpCodes = new OpCode[0x100];
            multiByteOpCodes = new OpCode[0x100];

            var fields = typeof(OpCodes).GetFields().ToList().Where(c => c.IsPublic);
            foreach (var field in fields)
            {
                var opCode = (OpCode)field.GetValue(null);

                var value = (ushort)opCode.Value;
                if (value < 0x100)
                {
                    singleByteOpCodes[value] = opCode;
                }
                else
                {
                    if ((value & 0xff00) != 0xfe00)
                        throw new Exception("Invalid OpCode.");

                    multiByteOpCodes[value & 0xff] = opCode;
                }
            }
        }

        /// <summary>If index lower then 0x100</summary>
        public static OpCode GetSingleByte(int index) => singleByteOpCodes[index];
        /// <summary>If index gretter then 0x100</summary>
        public static OpCode GetMultiByte(int index) => multiByteOpCodes[index];
    }
}

