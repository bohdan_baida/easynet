﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public abstract class Argument : IArgument
    {
        public string Name { get; set; }
        public Type Type { get; set; }

        public abstract bool IsApplicable(object value);
    }
}
