﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public interface IRangeArgument : IArgument
    {
        IComparable MinValue { get; }
        IComparable MaxValue { get; }
    }
}
