﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments.Abstract
{
    public interface IArgumentVaidator
    {
        bool Validate(IList<IArgument> arguments, params object[] parameters);
    }
}
