﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public class RangeArgument : Argument, IRangeArgument
    {
        public IComparable MinValue { get; set; }
        public IComparable MaxValue { get; set; }

        public override bool IsApplicable(object value)
        {
            if (!(value is IComparable))
                return false;

            var comparableValue = value as IComparable;

            if (this.MinValue.CompareTo(comparableValue) > 0)
                return false;

            if (this.MaxValue.CompareTo(comparableValue) < 0)
                return false;

            return true;
        }
    }
}
