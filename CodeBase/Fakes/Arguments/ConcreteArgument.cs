﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public class ConcreteArgument : Argument, IConcreteArgument
    {
        public object Value { get; set; }

        public override bool IsApplicable(object value)
        {
            return this.Value == value;
        }
    }
}
