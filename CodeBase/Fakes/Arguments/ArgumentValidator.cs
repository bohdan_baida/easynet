﻿using Fakes.Arguments.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public class ArgumentValidator : IArgumentVaidator
    {
        public bool Validate(IList<IArgument> arguments, params object[] parameters)
        {
            if (arguments.Count != parameters.Count())
                return false;

            for (int i = 0; i < arguments.Count; i++)
            {
                if (!arguments[i].IsApplicable(parameters[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
