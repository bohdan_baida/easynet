﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public class AnyArgument : Argument, IAnyArgument
    {
        public override bool IsApplicable(object value)
        {
            return this.Type == value.GetType();
        }
    }
}
