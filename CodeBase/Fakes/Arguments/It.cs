﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.Arguments
{
    public static class It
    {
        public static T IsAny<T>()
        {
            return default(T);
        }

        public static T IsInRange<T>(T a, T b)
            where T : IComparable
        {
            return default(T);
        }

    }
}
