﻿using Fakes.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup
{
    public class MemberSetupInfo
    {
        public MemberInfo Member { get; set; }

        public object Result { get; set; }
    }
}
