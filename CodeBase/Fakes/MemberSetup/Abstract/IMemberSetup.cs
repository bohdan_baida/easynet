﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup.Abstract
{
    public interface IMemberSetup<TMember, TResult>
    {
        void Returns(TResult result);
    }
}
