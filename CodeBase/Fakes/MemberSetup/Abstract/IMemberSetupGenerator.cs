﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup.Abstract
{
    public interface IMemberSetupGenerator
    {
        MemberSetupInfo Generate(Expression expression);
    }
}
