﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup.Abstract
{
    public interface IMembersSetup : IEnumerable<MemberSetupInfo>
    {
        void Add(MemberSetupInfo memberSetupInfo);
        MemberSetupInfo Find(string className, string memberName, params object[] parameters);

    }
}
