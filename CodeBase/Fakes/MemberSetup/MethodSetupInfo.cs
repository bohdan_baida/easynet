﻿using Fakes.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup
{
    public class MethodSetupInfo : MemberSetupInfo
    {
        public MethodSetupInfo()
        {
            Parameters = new List<IArgument>();
        }
        public IList<IArgument> Parameters { get; set; }
    }
}
