﻿using Fakes.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup.Generators
{
    public class MethodSetupCreator : ICreator<MethodCallExpression>
    {
        public MemberSetupInfo Create(MethodCallExpression expression)
        {
            var memberSetup = new MethodSetupInfo()
            {
                Member = expression.Method,
            };

            var args = expression.Method.GetParameters();
            foreach (var argument in expression.Arguments)
            {
                switch (argument.NodeType)
                {
                    case ExpressionType.Call:
                        var argMethod = (MethodCallExpression)argument;
                        if (argMethod.Method.Name == nameof(It.IsAny))
                        {
                            memberSetup.Parameters.Add(new AnyArgument()
                            {
                                Type = ((MethodCallExpression)argument).Method.GetGenericArguments().First(),
                                Name = args[expression.Arguments.IndexOf(argument)].Name
                            });
                        }
                        if (argMethod.Method.Name == nameof(It.IsInRange))
                        {
                            var rangeArgs = argMethod.Arguments;
                            memberSetup.Parameters.Add(new RangeArgument()
                            {
                                Type = ((MethodCallExpression)argument).Method.GetGenericArguments().First(),
                                Name = args[expression.Arguments.IndexOf(argument)].Name,
                                MinValue = (IComparable)((ConstantExpression)rangeArgs.First()).Value,
                                MaxValue = (IComparable)((ConstantExpression)rangeArgs.Last()).Value
                            });
                        }
                        break;
                    case ExpressionType.Constant:
                        memberSetup.Parameters.Add(new ConcreteArgument()
                        {
                            Value = ((ConstantExpression)argument).Value,
                            Type = ((ConstantExpression)argument).Type,
                            Name = args[expression.Arguments.IndexOf(argument)].Name
                        });
                        break;
                    default:
                        break;
                }
            }

            return memberSetup;
        }

        MemberSetupInfo ICreator.Create(Expression expression)
        {
            return Create((MethodCallExpression)expression);
        }
    }
}
