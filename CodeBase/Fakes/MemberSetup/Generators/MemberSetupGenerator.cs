﻿using Fakes.MemberSetup.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup.Generators
{
    public class MemberSetupGenerator : IMemberSetupGenerator
    {
        public static MemberSetupGenerator Instance { get; private set; }

        private MemberSetupGenerator()
        {

        }

        static MemberSetupGenerator()
        {
            Instance = new MemberSetupGenerator();
        }

        public MemberSetupInfo Generate(Expression expression)
        {
            if (expression is MethodCallExpression)
            {
#warning Should be injected
                return new MethodSetupCreator().Create(expression as MethodCallExpression);
            }

            if (expression is MemberExpression)
            {
                throw new NotImplementedException();
            }

            throw new ArgumentException("Following expression type is not handling");
        }

    }
}
