﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup.Generators
{
    public interface ICreator
    {
        MemberSetupInfo Create(Expression expression);
    }
    public interface ICreator<T> : ICreator
        where T: Expression
    {
        MemberSetupInfo Create(T expression);

    }
}
