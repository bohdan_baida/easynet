﻿using Fakes.MemberSetup;
using Fakes.MemberSetup.Abstract;
using Fakes.MemberSetup.Generators;
using System;
using System.Linq.Expressions;

namespace Fakes.TypeGeneration
{
    public class MemberSetup<TMember, TResult> : IMemberSetup<TMember, TResult>
    {
        public MemberSetupInfo MemberSetupInfo { get; private set; }

        private IMemberSetupGenerator memberSetupGenerator;

        public MemberSetup(Expression<Func<TMember, TResult>> expression, IMemberSetupGenerator memberSetupGenerator)
        {
            this.memberSetupGenerator = memberSetupGenerator;
            this.MemberSetupInfo = memberSetupGenerator.Generate(expression.Body);
        }
        public MemberSetup(Expression<Func<TMember, TResult>> expression)
            : this(expression, MemberSetupGenerator.Instance)
        {
        }

        public void Returns(TResult result)
        {
            MemberSetupInfo.Result = result;
        }
    }
}
