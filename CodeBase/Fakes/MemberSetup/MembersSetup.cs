﻿using Fakes.Arguments;
using Fakes.Arguments.Abstract;
using Fakes.MemberSetup.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakes.MemberSetup
{
    public class MembersSetup : IMembersSetup
    {
        IArgumentVaidator argumentValidator = new ArgumentValidator();

        private List<MemberSetupInfo> memberSetupInfos;

        public MembersSetup(IArgumentVaidator argumentVaidator)
        {
            this.argumentValidator = argumentVaidator;
            memberSetupInfos = new List<MemberSetupInfo>();
        }

        public MembersSetup() : this(new ArgumentValidator())
        {
        }

        public void Add(MemberSetupInfo memberSetupInfo)
        {
            memberSetupInfos.Add(memberSetupInfo);
        }

        public MemberSetupInfo Find(string className, string memberName, params object[] parameters)
        {
            // TODO : Should handle property setups also
            // Only methods are working for now
            return memberSetupInfos.FirstOrDefault(memberSetup =>
                    memberSetup.Member.DeclaringType.FullName == className &&
                    memberSetup.Member.ToString() == memberName &&
                    argumentValidator.Validate((memberSetup as MethodSetupInfo).Parameters.ToList(), parameters));
        }

        public IEnumerator<MemberSetupInfo> GetEnumerator()
        {
            return memberSetupInfos.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return memberSetupInfos.GetEnumerator();
        }
    }
}
