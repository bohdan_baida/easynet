﻿namespace UnitTesting.Generators
{
    public interface IObjectGenerator<T>
    {
        T Object { get; }
    }
}
