﻿using System;

namespace UnitTesting.Generators
{
    public static class GeneratorFactory
    {
        public static ObjectGenerator<TEntity> Create<TEntity>()
          where TEntity : class, new()
          => Activator.CreateInstance<ObjectGenerator<TEntity>>();
    }
}
