﻿using FluentAssertions;
using IOC.Enums;
using IOC.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace IOC.Tests
{
    public class Registry_UnitTests
    {
        class UserRepository : IUserRepository
        {
            public int A(int a, int b)
            {
                throw new NotImplementedException();
            }
        }
        class UserService { }
        interface IUserRepository {
            int A(int a, int b);
        }
        interface IUserService { }

        [Fact]
        public void AddTrancient_When_ImplementationNotAssignableFromAbstraction_Then_ShouldThrowException()
        {
            //Arrange
            var target = new Registry();

            //Act
            Action action = () => target.AddTrancient<IUserService, UserRepository>();

            //Assert
            action.Should()
                .Throw<InvalidOperationException>()
                .WithMessage($"Type {typeof(UserRepository).Name} is not assignable from {typeof(IUserService).Name}!");
        }

        [Fact]
        public void AddTrancient_When_TypeWasRegisteredBefore_Then_ShouldThrowException()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddTrancient<IUserRepository, UserRepository>();
            Action action = () => target.AddTrancient<IUserRepository, UserRepository>();

            //Assert
            action.Should()
                .Throw<InvalidOperationException>()
                .WithMessage($"You cannot add abastraction {typeof(IUserRepository).Name} twice!");
        }

        [Fact]
        public void AddTrancient_When_NewRegistryItemIsCorrect_Then_ShouldReturnNewRegistryItem()
        {
            //Arrange
            var target = new Registry();

            //Act
            var result = target.AddTrancient<IUserRepository, UserRepository>();

            //Assert
            result.Should().BeOfType<RegistryItem>();
            result.AbstractionType.Should().Be(typeof(IUserRepository));
            result.ImplementationType.Should().Be(typeof(UserRepository));
            result.Lifetime.Should().Be(ObjectLifetimesEnum.Trancient);

        }

        [Fact]
        public void AddTrancient_When_NewRegistryItemIsCorrect_Then_ShouldAddNewRegistryItem()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddTrancient<IUserRepository, UserRepository>();

            //Assert
            var registryItem = target.GetRegistry(typeof(IUserRepository));
            registryItem.Should().BeOfType<RegistryItem>();
            registryItem.AbstractionType.Should().Be(typeof(IUserRepository));
            registryItem.ImplementationType.Should().Be(typeof(UserRepository));
            registryItem.Lifetime.Should().Be(ObjectLifetimesEnum.Trancient);

        }

        [Fact]
        public void AddSingleton_When_ImplementationNotAssignableFromAbstraction_Then_ShouldThrowException()
        {
            //Arrange
            var target = new Registry();

            //Act
            Action action = () => target.AddSingleton<IUserService, UserRepository>();

            //Assert
            action.Should()
                .Throw<InvalidOperationException>()
                .WithMessage($"Type {typeof(UserRepository).Name} is not assignable from {typeof(IUserService).Name}!");
        }

        [Fact]
        public void AddSingleton_When_TypeWasRegisteredBefore_Then_ShouldThrowException()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            Action action = () => target.AddSingleton<IUserRepository, UserRepository>();

            //Assert
            action.Should()
                .Throw<InvalidOperationException>()
                .WithMessage($"You cannot add abastraction {typeof(IUserRepository).Name} twice!");
        }

        [Fact]
        public void AddSingleton_When_NewRegistryItemIsCorrect_Then_ShouldReturnNewRegistryItem()
        {
            //Arrange
            var target = new Registry();

            //Act
            var result = target.AddSingleton<IUserRepository, UserRepository>();

            //Assert
            result.Should().BeOfType<RegistryItem>();
            result.AbstractionType.Should().Be(typeof(IUserRepository));
            result.ImplementationType.Should().Be(typeof(UserRepository));
            result.Lifetime.Should().Be(ObjectLifetimesEnum.Singleton);

        }

        [Fact]
        public void AddSingleton_When_NewRegistryItemIsCorrect_Then_ShouldAddNewRegistryItem()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();

            //Assert
            var registryItem = target.GetRegistry(typeof(IUserRepository));
            registryItem.Should().BeOfType<RegistryItem>();
            registryItem.AbstractionType.Should().Be(typeof(IUserRepository));
            registryItem.ImplementationType.Should().Be(typeof(UserRepository));
            registryItem.Lifetime.Should().Be(ObjectLifetimesEnum.Singleton);

        }

        [Fact]
        public void GetRegistry_When_InputParameterIsNull_Then_Should_ReturnNull()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.GetRegistry(null);

            //Assert
            result.Should().BeNull();
        }

        [Fact]
        public void GetRegistry_When_TypeIsNotRegistered_Then_Should_ReturnNull()
        {
            //Arrange
            var targetType = typeof(IUserService);
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.GetRegistry(targetType);

            //Assert
            result.Should().BeNull();
        }

        [Fact]
        public void GetRegistry_When_TypeIsRegistered_Then_Should_ReturnRegistry()
        {
            //Arrange
            var targetType = typeof(IUserRepository);
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.GetRegistry(targetType);

            //Assert
            result.Should().BeOfType<RegistryItem>();
            result.AbstractionType.Should().Be(targetType);
        }

        [Fact]
        public void ContainsRegistry_When_InputParameterIsNull_Then_Should_ReturnFalse()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.ContainsRegistry(null);

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void ContainsRegistry_When_TypeIsNotInRegistry_Then_Should_ReturnFalse()
        {
            //Arrange
            var targetType = typeof(IUserService);
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.ContainsRegistry(targetType);

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void ContainsRegistry_When_TypeInRegistry_Then_Should_ReturnTrue()
        {
            //Arrange
            var targetType = typeof(IUserRepository);
            var target = new Registry();
            var mock = new Mock<IUserRepository>();
            mock.Setup(c => c.A(1, It.IsAny<int>())).Returns(1);
                
            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.ContainsRegistry(targetType);

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void GetResolver_When_ReturnedTypeResolver_Then_ShouldHaveRegistryEqualsTarget()
        {
            //Arrange
            var target = new Registry();

            //Act
            target.AddSingleton<IUserRepository, UserRepository>();
            var result = target.GetResolver();

            //Assert
            result.Should().BeAssignableTo<ITypeResolver>();
            result.Configuration.Should().Equals(target);
        }
    }
}
